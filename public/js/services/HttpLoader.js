module.exports 				= function ($q, $http)
{
	var baseUrl 			= "https://mahjongmayhem.herokuapp.com";

	function get (url)
	{
		var defer 			= $q.defer();

		$http.get(baseUrl + url, { cache: 'true' })
		.success(function (response)
		{
			defer.resolve(response);
		});

		return defer.promise;
	}

	function post (url, data)
	{
		var defer 			= $q.defer();

		$http.post(baseUrl + url, JSON.stringify(data))
		.success(function (response, status)
		{
			defer.resolve(response, status);
		}).error(function (response, status)
		{
			defer.resolve(response, status);
		});

		return defer.promise;
	}

	return {
		get: function (url)
		{
			return get(url);
		},
		post: function (url, data)
		{
			return post(url, data);
		}
	};
};