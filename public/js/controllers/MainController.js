/*module.exports					= function ($scope)
{*/
angular.module('MainCtrl', []).controller('MainController', function($scope, CipherService) {

	$scope.formData = {
		encryptedMessage: {
			cipher_text: ''
		},
		decryptedMessage: 'message',
		secret: ''
	};

	$scope.encrypt = function(data) {
		if(data.secret && data.name){
    		this.formData.encryptedMessage = CipherService.encrypt('message', data.name + data.secret);
		} else {
			alert("Please enter a password and a username");
		}
    }

    $scope.decrypt = function(data) {
    	if(data.secret && data.name){
    		var msg = CipherService.decrypt(this.formData.encryptedMessage.cipher_text, data.name + data.secret, this.formData.encryptedMessage.salt, this.formData.encryptedMessage.iv);
    		this.formData.decryptedMessage = msg;
		} else {
			alert("Please enter a password and a username");
		}
    }
});